import Head from "next/head";

export default ({ title }) => {
  return (
    <Head>
      <title>{title} - PalpiteBox</title>
    </Head>
  );
};
