import { GoogleSpreadsheet } from "google-spreadsheet";
import moment from "moment";
import { fromBase64 } from "../../utils/base64";

const doc = new GoogleSpreadsheet(process.env.DOC_ID);

function generateCoupon() {
  const code = parseInt(moment().format("YYMMDDHHmmssSSS"))
    .toString(16)
    .toUpperCase();
  return code.substr(0, 4) + "-" + code.substr(4, 4) + "-" + code.substr(8, 4);
}

export default async (req, res) => {
  try {
    await doc.useServiceAccountAuth({
      client_email: process.env.CLIENT_EMAIL,
      private_key: fromBase64(process.env.PRIVATE_KEY),
    });
    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[1];
    const data = JSON.parse(req.body);

    const sheetPromo = doc.sheetsByIndex[2];
    await sheetPromo.loadCells("A4:B4");

    const ativarPromocao = sheetPromo.getCell(3, 0);
    const textorPromocao = sheetPromo.getCell(3, 1);

    let Cupom = "";
    let Promo = "";

    if (ativarPromocao.value === "VERDADEIRO") {
      (Cupom = generateCoupon()), (Promo = textorPromocao.value);
    }

    await sheet.addRow({
      Nome: data.Nome,
      Email: data.Email,
      Whatsapp: data.Whatsapp,
      Cupom,
      Promo,
      "Data de Preenchimento": moment().format("DD/MM/YYYY - HH:mm:ss"),
      Nota: data.Nota,
    });

    res.json({
      showCupom: Cupom !== "",
      Cupom,
      Promo,
    });
  } catch (error) {
    console.log(error);
    res.end(error);
  }
};
