import PageTitle from "../components/PageTitle";

export default () => {
  return (
    <>
      <PageTitle title="Contato" />

      <div className="w-full sm:w-8/12 p-8 text-center m-auto">
        <h1 className="uppercase font-semibold text-4xl mb-8">Contato</h1>

        <div className="w-full m-auto">
          <p className="text-lg text-gray-800">
            Todos os contados estão localizados no rodapé.
          </p>
        </div>
      </div>
    </>
  );
};
