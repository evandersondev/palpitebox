export default () => {
  return (
    <div className="py-4 mt-auto bg-gray-600">
      <div className="container">
        <p className="text-gray-900 text-center font-medium">
          Projeto desenvolvido por:{" "}
          <a className="hover:underline text-teal-800" target="_blank" href="http://evandersondev.netlify.app">
            EvandersonDev
          </a>{" "}|{" "}
          <a className="hover:underline text-teal-800"  target="_blank" href="http://www.linkedin.com/in/evandersondev">
            LinkedIn
          </a>{" "}|{" "}
          <a className="hover:underline text-teal-800"  target="_blank" href="http://www.github.com/evandersondev">
            Github
          </a>{" "}
          
          <img className="inline w-32 p-4" src="logo_semana_fsm.png" alt="Semana Fullstack Master"/>
          <img className="inline w-32 p-4" src="logo_devpleno.png" alt="DevPleno"/>
        </p>
      </div>
    </div>
  )
}