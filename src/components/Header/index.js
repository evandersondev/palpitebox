import Link from 'next/link'
import { wrapper, container, logo } from './styles.module.css'

export default () => {
  return (
    <div className={wrapper}>
      <header className={container}>
        <Link href="/">
          <a><img className={logo} src="logo.png" alt="palpitebox logo"/></a>
        </Link>
      </header>

      <div className="mt-6 text-center">
        <Link href="sobre" >
          <a className="px-2 font-bold text-gray-800 hover:underline">Sobre</a>
        </Link>

        <Link href="contato" >
          <a className="px-2 font-bold text-gray-800 hover:underline">Contato</a>
        </Link>

        <Link href="pesquisa" >
          <a className="px-2 font-bold text-gray-800 hover:underline">Pesquisa</a>
        </Link>
      </div>
    </div>
  )
}