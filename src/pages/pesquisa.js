import { useState } from "react";
import PageTitle from "../components/PageTitle";
import Link from "next/link";

export default () => {
  const [form, setForm] = useState({});
  const [success, setSuccess] = useState(false);
  const [returnResponse, setReturnResponse] = useState({});
  const notas = [0, 1, 2, 3, 4, 5];

  function formChange(event) {
    const value = event.target.value;
    const key = event.target.name;

    setForm((old) => ({
      ...old,
      [key]: value,
    }));
  }

  async function handleSubimit(event) {
    event.preventDefault();

    try {
      const response = await fetch("/api/save", {
        method: "POST",
        body: JSON.stringify({ ...form, Nota: parseInt(form.Nota) }),
      });

      const data = await response.json();

      setSuccess(true);
      setReturnResponse(data);
    } catch (error) {
      setSucess(false);
      console.log(error);
    }
  }

  return (
    <>
      <PageTitle title="Pesquisa" />

      <div className="mx-auto p-8 sm:w-2/4 w-full">
        <h1 className="text-2xl text-center font-semibold py-8 text-gray-900">
          Críticas e sugestões
        </h1>
        <p className="text-center text-gray-700 font-medium text-lg">
          O restaurante X sempre busca por atender melhor seus clientes. Por
          isso, estamos sempre abertos a ouvir a sua opinião.
        </p>

        {!success && (
          <form onSubmit={handleSubimit} className="w-full p-12 text-center">
            <fieldset className="flex flex-col w-full mx-auto mb-4">
              <label
                className="text-gray-900 text-left font-medium"
                htmlFor="name"
              >
                Seu nome:
              </label>
              <input
                onChange={formChange}
                name="Nome"
                value={form.Nome}
                className="w-full p-4 h-10 border border-gray-500 rounded"
                type="text"
                id="name"
              />
            </fieldset>

            <fieldset className="flex flex-col w-full mx-auto mb-4">
              <label
                className="text-gray-900 text-left
            font-medium"
                htmlFor="email"
              >
                E-mail:
              </label>
              <input
                onChange={formChange}
                name="Email"
                value={form.Email}
                className="w-full p-4 h-10 border border-gray-500 rounded"
                type="email"
                id="email"
              />
            </fieldset>

            <fieldset className="flex flex-col w-full mx-auto mb-4">
              <label
                className="text-gray-900 text-left font-medium"
                htmlFor="whats"
              >
                Whatsapp:
              </label>
              <input
                onChange={formChange}
                name="Whatsapp"
                value={form.Whatsapp}
                className="w-full p-4 h-10 border border-gray-500 rounded"
                type="text"
                id="whats"
              />
            </fieldset>

            <fieldset className="flex flex-col w-full mx-auto mb-4">
              <label className="text-gray-900 text-left font-medium">
                Nota:
              </label>
              <div className="flex justify-center w-full mt-4">
                {notas.map((nota) => (
                  <labe className="block w-1/6 text-center">
                    {nota}
                    <br />
                    <input
                      onChange={formChange}
                      name="Nota"
                      value={nota}
                      className="w-3 h-3 inline"
                      type="radio"
                      id="nota"
                    />
                  </labe>
                ))}
              </div>
            </fieldset>

            {/* <fieldset className="flex flex-col w-full mx-auto mb-4">
          <label
            className="text-gray-900 text-left font-medium"
            htmlFor="pesquisa"
          >
            Sua crítica ou sugestão:
          </label>
          <textarea
           onChange={formChange}
           name="Whatsapp"
           value={form.Whatsapp}
            className="w-full p-4 h-32 border border-gray-500 rounded"
            type="text"
            id="pesquisa"
            rows="5"
            wrap="soft"
          />
        </fieldset> */}

            <button
              className="bg-blue-600 px-6 py-2 rounded shadow-lg font-bold text-white hover:bg-blue-700 transition duration-200 w-full mx-auto"
              type="submit"
            >
              Enviar
            </button>
          </form>
        )}
        {success && (
          <div className="w-full mt-4 text-center">
            <p className="p-8 mb-4 text-lg bg-green-600 rounded text-white">
              Obrigado por contribuir por sua sugestão ou crítica.
            </p>

            {returnResponse.showCupom && (
              <>
                <div className="text-center mb-4 text-xl border border-gray-400 rounded p-6 ">
                  Seu cupom: <br />
                  <span className="font-bold text-2xl">
                    {returnResponse.Cupom}
                  </span>
                </div>

                <div className="text-center border border-gray-400 rounded p-6 ">
                  <span className="text-gray-900 block mb-2 font-medium">
                    {returnResponse.Promo}
                  </span>
                  <br />
                  <p className="italic">
                    Tire um print ou foto desta tela e apresente ao garçon
                  </p>
                </div>
              </>
            )}
          </div>
        )}
      </div>
    </>
  );
};
