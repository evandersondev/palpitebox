import { GoogleSpreadsheet } from "google-spreadsheet";
import { fromBase64 } from "../../utils/base64";

const doc = new GoogleSpreadsheet(process.env.DOC_ID);

export default async (req, res) => {
  try {
    await doc.useServiceAccountAuth({
      client_email: process.env.CLIENT_EMAIL,
      private_key: fromBase64(process.env.PRIVATE_KEY),
    });
    await doc.loadInfo();

    const sheet = doc.sheetsByIndex[2];
    await sheet.loadCells("A4:B4");

    const ativarPromocao = sheet.getCell(3, 0);
    const textorPromocao = sheet.getCell(3, 1);

    res.json({
      showCoupon: ativarPromocao.value === "VERDADEIRO",
      message: textorPromocao.value,
    });
  } catch (err) {
    res.json({
      showCoupon: false,
      message: "",
    });
  }
};
