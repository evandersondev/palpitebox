import Layout from '../components/Layout'
import '../css/styles.css'

export default ({Component, pageProps}) => {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>    
  )
}