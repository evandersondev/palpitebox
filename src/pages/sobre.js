import PageTitle from "../components/PageTitle";

export default () => {
  return (
    <>
      <PageTitle title="Sobre" />
      <div className="w-full sm:w-8/12 p-8 text-center m-auto">
        <h1 className="uppercase font-semibold text-4xl mb-8">Sobre</h1>
        <div className="w-full m-auto">
          <p className="text-lg text-gray-800">
            Projeto criado no curso FullStack Master - DevPleno, este projeto
            simula um sistems para restaurantes de feedback dos clientes, ao dar
            um feedback o cliente recebe um desconto caso naquele momento exista
            um desconto ativado.
          </p>
        </div>
      </div>
    </>
  );
};
