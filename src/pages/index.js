import Link from "next/link";
import useSWR from "swr";

import PageTitle from "../components/PageTitle";

export default () => {
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data, error } = useSWR("/api/get-promo", fetcher);

  return (
    <>
      <PageTitle title="Seja bem vindo" />

      <div>
        <p className="text-lg my-12 text-center">
          O restaurante X sempre busca por atender melhor seus clientes. Por
          isso, estamos sempre abertos a ouvir a sua opinião.
        </p>

        <div className="text-center my-12">
          <Link href="/pesquisa">
            <a className="bg-blue-600 px-6 py-4 rounded shadow-lg font-bold text-white hover:bg-blue-700 transition duration-200">
              Dar opinião ou sugestão
            </a>
          </Link>
        </div>

        {!data && <p className="text-center m-6">carregando...</p>}
        {!error && data && data.showCoupon && (
          <p className="text-center m-6">{data.message}</p>
        )}
      </div>
    </>
  );
};
