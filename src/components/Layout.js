import Header from "./Header";
import Footer from "./Footer";

export default ({ children }) => (
  <>
    <Header />
      <div className="container mx-auto">
        {children}
      </div>
    <Footer />
  </>
)